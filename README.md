# Making and breaking electronic structures: Opportunities from machine learning and orbital-free embedding

Talk at the "Machine Learning, Embedding, and Dynamics of Many-Electron States in Molecules and Materials" symposium at the CUNY graduate center, (10/20/2023).

## To fully enjoy it

Make sure to `pip install dftpy qepy`


## Contact

Michele Pavanello
m.pavanello@rutgers.edu
@MikPavanello
